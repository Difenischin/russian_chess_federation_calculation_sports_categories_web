'use strict';

var gulp = require('gulp'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    gutil = require('gulp-util'),
    notify = require("gulp-notify"),
    plumber = require('gulp-plumber'),
    concat  = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    uglify  = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename = require('gulp-rename'),
    path = require('path');

const LESS_DIR = './web-dev/less',
    CSS_DIR = './web-dev/css/',
    AUTOPREFIXER_RULES = [
        'last 15 versions'
    ];

var handleError = function (err) {
    gutil.beep();
    gutil.log(err);
    this.emit('end');
    gulp.src(err.filename)
        .pipe(notify({
            title: path.basename(err.filename) + ' (' + err.line + ':' + err.column + ')',
            message: err.message
        }));
};

gulp.task('less', function () {
    gulp.src(LESS_DIR + '/**/*')
        .pipe(plumber({errorHandler: handleError}))
        .pipe(less())
        .pipe(autoprefixer(AUTOPREFIXER_RULES))
        .pipe(gulp.dest(CSS_DIR));
});

gulp.task('scripts', function() {
    return gulp.src([ 
        './web-dev/js/jquery-3.1.1.min.js', 
        './web-dev/js/yii.js', 
        './web-dev/js/yii.activeForm.js', 
        './web-dev/js/yii.captcha.js', 
        './web-dev/js/yii.validation.js', 
        './web-dev/js/yii.gridView.js',
        './web-dev/js/bootstrap.js',
        './web-dev/js/datatables.js',
        './web-dev/js/site.js', 
        ])
        .pipe(concat('site.min.js'))
        .pipe(uglify()) // Сжимаем JS файл
        .pipe(gulp.dest('./web/js')); // Выгружаем в папку app/js
});
gulp.task('css-libs', ['less'], function() {
    return gulp.src([ 
        './web-dev/css/bootstrap.css', 
        './web-dev/css/site.css',
        './web-dev/css/datatables.min.css',
        ])
        .pipe(concat('site.css'))
        .pipe(cssnano()) // Сжимаем
        .pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
        .pipe(gulp.dest('./web/css')); // Выгружаем в папку app/css
});
gulp.task('default', function() {  
    gulp.run('less', 'scripts', 'css-libs');
});
gulp.task('watch', function () {
    gulp.watch(LESS_DIR + '/**/*', ['less']);
});