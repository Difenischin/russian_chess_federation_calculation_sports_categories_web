<?php
use yii\helpers\Html;
use app\components\ruchess\SportsCategories;

?>

<div id="norma-count-table">
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
        .tg .tg-8k1q{font-size:11px;font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif !important;;text-align:center;vertical-align:top}
        .tg .tg-n4zk{font-size:11px;font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif !important;;vertical-align:top}
    </style>
    <table class="tg table-bordered" border="1" cellpadding="0" cellspacing="0">
        <tr>
            <th class="tg-8k1q" colspan="3" rowspan="2">КМС</th>
            <th class="tg-8k1q" colspan="9">Спортивные разряды</th>
            <th class="tg-8k1q" colspan="6">Юношеские спортивные разряды</th>
        </tr>
        <tr>
            <td class="tg-8k1q" colspan="3">I</td>
            <td class="tg-8k1q" colspan="3">II</td>
            <td class="tg-8k1q" colspan="3">III</td>
            <td class="tg-8k1q" colspan="2">I</td>
            <td class="tg-8k1q" colspan="2">II</td>
            <td class="tg-8k1q" colspan="2">III</td>
        </tr>
        <tr>
            <td class="tg-8k1q" colspan="2">Средний рейтинг соперников</td>
            <td class="tg-8k1q" rowspan="2">Норма (очки)</td>
            <td class="tg-8k1q" colspan="2">Средний рейтинг соперников</td>
            <td class="tg-8k1q" rowspan="2">Норма (очки)</td>
            <td class="tg-8k1q" colspan="2">Средний рейтинг соперников</td>
            <td class="tg-8k1q" rowspan="2">Норма (очки)</td>
            <td class="tg-8k1q" colspan="2">Средний рейтинг соперников</td>
            <td class="tg-8k1q" rowspan="2">Норма (очки)</td>
            <td class="tg-8k1q" rowspan="2">Средний рейтинг соперников</td>
            <td class="tg-8k1q" rowspan="2">Норма (очки)</td>
            <td class="tg-8k1q" rowspan="2">Средний рейтинг соперников</td>
            <td class="tg-8k1q" rowspan="2">Норма (очки)</td>
            <td class="tg-8k1q" rowspan="2">Средний рейтинг соперников</td>
            <td class="tg-8k1q" rowspan="2">Норма (очки)</td>
        </tr>
        <tr>
            <td class="tg-8k1q">М</td>
            <td class="tg-8k1q">Ж</td>
            <td class="tg-8k1q">М</td>
            <td class="tg-8k1q">Ж</td>
            <td class="tg-8k1q">М</td>
            <td class="tg-8k1q">Ж</td>
            <td class="tg-8k1q">М</td>
            <td class="tg-8k1q">Ж</td>
        </tr>
        <tr>
            <td class="tg-8k1q">1901-1925</td>
            <td class="tg-8k1q">1801-1825</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.75, $countRaund); ?> (75%)</td>
            <td class="tg-8k1q">1701-1725</td>
            <td class="tg-8k1q">1601-1625</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.75, $countRaund); ?> (75%)</td>
            <td class="tg-8k1q">1501-1525</td>
            <td class="tg-8k1q">1401-1425</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.75, $countRaund); ?> (75%)</td>
            <td class="tg-8k1q">1301-1325</td>
            <td class="tg-8k1q">1201-1225</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.75, $countRaund); ?> (75%)</td>
            <td class="tg-8k1q">1151-1156</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.75, $countRaund); ?> (75%)</td>
            <td class="tg-8k1q">1101-1106</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.75, $countRaund); ?> (75%)</td>
            <td class="tg-8k1q"></td>
            <td class="tg-n4zk"></td>
        </tr>
        <tr>
            <td class="tg-8k1q">1926-1950</td>
            <td class="tg-8k1q">1826-1850</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.7, $countRaund); ?> (70%)</td>
            <td class="tg-8k1q">1726-1750</td>
            <td class="tg-8k1q">1626-1650</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.7, $countRaund); ?> (70%)</td>
            <td class="tg-8k1q">1526-1550</td>
            <td class="tg-8k1q">1426-1450</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.7, $countRaund); ?> (70%)</td>
            <td class="tg-8k1q">1326-1350</td>
            <td class="tg-8k1q">1226-1250</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.7, $countRaund); ?> (70%)</td>
            <td class="tg-8k1q">1157-1162</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.7, $countRaund); ?> (70%)</td>
            <td class="tg-8k1q">1107-1112</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.7, $countRaund); ?> (70%)</td>
            <td class="tg-8k1q"></td>
            <td class="tg-n4zk"></td>
        </tr>
        <tr>
            <td class="tg-8k1q">1951-1975</td>
            <td class="tg-8k1q">1851-1875</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.65, $countRaund); ?> (65%)</td>
            <td class="tg-8k1q">1751-1775</td>
            <td class="tg-8k1q">1651-1675</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.65, $countRaund); ?> (65%)</td>
            <td class="tg-8k1q">1551-1575</td>
            <td class="tg-8k1q">1451-1475</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.65, $countRaund); ?> (65%)</td>
            <td class="tg-8k1q">1351-1375</td>
            <td class="tg-8k1q">1251-1275</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.65, $countRaund); ?> (65%)</td>
            <td class="tg-8k1q">1163-1168</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.65, $countRaund); ?> (65%)</td>
            <td class="tg-8k1q">1113-1118</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.65, $countRaund); ?> (65%)</td>
            <td class="tg-8k1q"></td>
            <td class="tg-n4zk"></td>
        </tr>
        <tr>
            <td class="tg-8k1q">1976-2000</td>
            <td class="tg-8k1q">1876-1900</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.6, $countRaund); ?> (60%)</td>
            <td class="tg-8k1q">1776-1800</td>
            <td class="tg-8k1q">1676-1700</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.6, $countRaund); ?> (60%)</td>
            <td class="tg-8k1q">1576-1600</td>
            <td class="tg-8k1q">1476-1500</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.6, $countRaund); ?> (60%)</td>
            <td class="tg-8k1q">1376-1400</td>
            <td class="tg-8k1q">1276-1300</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.6, $countRaund); ?> (60%)</td>
            <td class="tg-8k1q">1169-1174</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.6, $countRaund); ?> (60%)</td>
            <td class="tg-8k1q">1119-1124</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.6, $countRaund); ?> (60%)</td>
            <td class="tg-8k1q">1000</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.6, $countRaund); ?> (60%)</td>
        </tr>
        <tr>
            <td class="tg-8k1q">2001-2025</td>
            <td class="tg-8k1q">1901-1925</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.55, $countRaund); ?> (55%)</td>
            <td class="tg-8k1q">1801-1825</td>
            <td class="tg-8k1q">1701-1725</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.55, $countRaund); ?> (55%)</td>
            <td class="tg-8k1q">1601-1625</td>
            <td class="tg-8k1q">1501-1525</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.55, $countRaund); ?> (55%)</td>
            <td class="tg-8k1q">1401-1425</td>
            <td class="tg-8k1q">1301-1325</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.55, $countRaund); ?> (55%)</td>
            <td class="tg-8k1q">1175-1180</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.55, $countRaund); ?> (55%)</td>
            <td class="tg-8k1q">1125-1130</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.55, $countRaund); ?> (55%)</td>
            <td class="tg-8k1q">1001-1025</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.55, $countRaund); ?> (55%)</td>
        </tr>
        <tr>
            <td class="tg-8k1q">2026-2050</td>
            <td class="tg-8k1q">1926-1950</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.50, $countRaund); ?> (50%)</td>
            <td class="tg-8k1q">1826-1850</td>
            <td class="tg-8k1q">1726-1750</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.50, $countRaund); ?> (50%)</td>
            <td class="tg-8k1q">1626-1650</td>
            <td class="tg-8k1q">1526-1550</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.50, $countRaund); ?> (50%)</td>
            <td class="tg-8k1q">1426-1450</td>
            <td class="tg-8k1q">1326-1350</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.50, $countRaund); ?> (50%)</td>
            <td class="tg-8k1q">1181-1185</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.50, $countRaund); ?> (50%)</td>
            <td class="tg-8k1q">1131-1135</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.50, $countRaund); ?> (50%)</td>
            <td class="tg-8k1q">1026-1050</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.50, $countRaund); ?> (50%)</td>
        </tr>
        <tr>
            <td class="tg-8k1q">2051-2075</td>
            <td class="tg-8k1q">1951-1975</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.45, $countRaund); ?> (45%)</td>
            <td class="tg-8k1q">1851-1875</td>
            <td class="tg-8k1q">1751-1775</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.45, $countRaund); ?> (45%)</td>
            <td class="tg-8k1q">1651-1675</td>
            <td class="tg-8k1q">1551-1575</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.45, $countRaund); ?> (45%)</td>
            <td class="tg-8k1q">1451-1475</td>
            <td class="tg-8k1q">1351-1375</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.45, $countRaund); ?> (45%)</td>
            <td class="tg-8k1q">1186-1190</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.45, $countRaund); ?> (45%)</td>
            <td class="tg-8k1q">1136-1140</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.45, $countRaund); ?> (45%)</td>
            <td class="tg-8k1q">1051-1075</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.45, $countRaund); ?> (45%)</td>
        </tr>
        <tr>
            <td class="tg-8k1q">2076-2100</td>
            <td class="tg-8k1q">1976-2000</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.4, $countRaund); ?> (40%)</td>
            <td class="tg-8k1q">1876-1900</td>
            <td class="tg-8k1q">1776-1800</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.4, $countRaund); ?> (40%)</td>
            <td class="tg-8k1q">1676-1700</td>
            <td class="tg-8k1q">1576-1600</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.4, $countRaund); ?> (40%)</td>
            <td class="tg-8k1q">1476-1500</td>
            <td class="tg-8k1q">1376-1400</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.4, $countRaund); ?> (40%)</td>
            <td class="tg-8k1q">1191-1200</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.4, $countRaund); ?> (40%)</td>
            <td class="tg-8k1q">1141-1150</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.4, $countRaund); ?> (40%)</td>
            <td class="tg-8k1q">1076-1100</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.4, $countRaund); ?> (40%)</td>
        </tr>
        <tr>
            <td class="tg-8k1q">&gt;                2100</td>
            <td class="tg-8k1q">&gt;              2000</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.35, $countRaund); ?> (35%)</td>
            <td class="tg-8k1q">&gt;       1900</td>
            <td class="tg-8k1q">&gt;            1800</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.35, $countRaund); ?> (35%)</td>
            <td class="tg-8k1q">&gt;        1700</td>
            <td class="tg-8k1q">&gt;          1600</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.35, $countRaund); ?> (35%)</td>
            <td class="tg-8k1q">&gt;                     1500</td>
            <td class="tg-8k1q">&gt;                1400</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.35, $countRaund); ?> (35%)</td>
            <td class="tg-8k1q">&gt;1200</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.35, $countRaund); ?> (35%)</td>
            <td class="tg-8k1q">&gt;1150</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.35, $countRaund); ?> (35%)</td>
            <td class="tg-8k1q">&gt;1100</td>
            <td class="tg-8k1q"><?php echo SportsCategories::getNormaPoints(0.35, $countRaund); ?> (35%)</td>
        </tr>
    </table>
</div>

<style>
    table.table-bordered span {
        font-size: 11px;
    }
</style>