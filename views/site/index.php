<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Подсчёт спортивных разрядов по шахматам';
$this->registerMetaTag(['name' => 'title', 'content' => 'Подсчёт спортивных разрядов по шахматам']);
$this->registerMetaTag(['name' => 'description', 'content' => 'Подсчёт спортивных разрядов по шахматам, в турнирах обсчитываемых российской шахматной федерацией.']);
$this->registerMetaTag(['name' => 'keywords', 'content' => 'разряды шахматы, какой я выполнил разряд, какой я выполнил разряд по шахматам, шахматы разряды, разряды по ршф, разряды по РШФ, разряды ршф, массовые разряды шахматы, шахматные разряды, нормы выполнения шахматных разрядов, if[vfns hfphzls,hfphzls gj if[vfnfv, rfrjq z dsgjkybk hfphzl']);
?>
<div class="site-index">
    <div class="jumbotron">

        <h2>Для расчёта спортивных разрядов по шахматам вставьте ссылку на турнир с сайта
            <a href="http://ratings.ruchess.ru/tournaments">РШФ</a> в поле ниже или выберите из ранее рассчитанных
            турниров</h2>

        <?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => ['/'], 'options' => ['method' => 'post']]); ?>
        <?= $form->field($model, 'tournament_url')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Рассчитать разряды', ['class' => 'btn btn-lg btn-success', 'name' => 'contact-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


    <div class="body-content">

        <div class="last-tournaments">
            <?php
            if (count($lastTournament) > 0) {
                echo Html::tag('h2', 'Последние турниры, по которым рассчитывался разряд');
                echo Html::beginTag('ul class="nav nav-pills nav-stacked"');
                foreach ($lastTournament as $t) {
                    echo Html::beginTag("li");
                    $text = "$t->name ( $t->city )";
                    echo Html::tag('a href="#" name=' . $t->tournament_url . '', $text);
                    echo Html::endTag("li");
                }
                echo Html::endTag('ul');
            }
            ?>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Правила присвоения шахматных разрядов</h2>

                <p>Правила присвоения разрядов по шахматам опубликованы на официальном сайте Министерства спорта
                    Российской Федерации, в разделе спорт высших достижений</p>
                <a href="https://minsport.gov.ru/sport/high-sport/edinaya-vserossiyska/31598/">ЕВСК 2018 - 2021 г</a>

                <p><a class="btn btn-default"
                      href="http://www.minsport.gov.ru/sport/high-sport/edinaya-vserossiyska/5507/">Правила &raquo;</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Рейтинговые турниры РШФ</h2>
                <p>Для просмотра списка шахматных турниров обсчитываемых российской шахматной федерацией, нажмите на
                    кнопку
                    ниже</p>
                <p><a class="btn btn-default" href="http://ratings.ruchess.ru/tournaments">Турниры РШФ &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Расчёт шахматных разрядов</h2>

                <p>Данный сервис находиться на этапе РАЗРАБОТКИ, возможны неточности в расчётах. Если Вы нашли ошибку -
                    прошу написать на электронный адрес.</p>
                <p>Расчёт производится согласно ЕВСК 2018 - 2021 г.</p>
                <p>В выводимых результатах не учитывается ограничение на количество туров, необходимое для присвоения
                    разрядов</p>
                <p>Не обсчитываются шахматные турниры проводимые по круговой системе.</p>
                <p>Есть предложение что добавить/удалить/изменить? - Пиши на электронный адрес.</p>
            </div>
        </div>
    </div>
</div>