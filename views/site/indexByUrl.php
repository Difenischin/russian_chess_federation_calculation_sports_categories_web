<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Подсчёт спортивных разрядов по шахматам';
$this->registerMetaTag(['name' => 'title', 'content' => 'Подсчёт спортивных разрядов по шахматам']);
$this->registerMetaTag(['name' => 'description', 'content' => 'Подсчёт спортивных разрядов по шахматам, в турнирах обсчитываемых российской шахматной федерацией.']);
$this->registerMetaTag(['name' => 'keywords', 'content' => 'разряды шахматы, какой я выполнил разряд, какой я выполнил разряд по шахматам, шахматы разряды, разряды по ршф, разряды по РШФ, разряды ршф, массовые разряды шахматы, шахматные разряды, нормы выполнения шахматных разрядов, if[vfns hfphzls,hfphzls gj if[vfnfv, rfrjq z dsgjkybk hfphzl']);
?>
<div class="site-index">
    <div class="jumbotron">

        <h2>Для расчёта спортивных разрядов по шахматам вставьте ссылку на турнир с сайта
            <a href="http://ratings.ruchess.ru/tournaments">РШФ</a> в поле ниже</h2>

        <?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => ['/'], 'options' => ['method' => 'post']]); ?>
        <?= $form->field($model, 'tournament_url')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Рассчитать разряды', ['class' => 'btn btn-lg btn-success', 'name' => 'contact-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


    <div class="body-content">

        <div class="last-tournaments">
            <?php
            if (count($tournamentPlayers) < 5) {
                echo Html::tag('h2', 'Последние турниры, по которым рассчитывался разряд');
                echo Html::beginTag('ul class="nav nav-pills nav-stacked"');
                foreach ($lastTournament as $t) {
                    echo Html::beginTag("li");
                    echo Html::tag('a href="#" name=' . $t->tournament_url . '', $t->name);
                    echo Html::endTag("li");
                }
                echo Html::endTag('ul');
            }
            ?>
        </div>
        <?php
        if (count($tournamentPlayers) > 5) {
            echo Html::tag("h1", $tournamentName);
            echo($tournamentInfo);

            echo Html::beginTag('table id="categories" class="table"');
            echo Html::beginTag('thead');
            echo Html::beginTag('tr');
            echo Html::tag('th', 'Ст. №');
            echo Html::tag('th', 'Имя участника');
            echo Html::tag('th', 'Rнач');
            echo Html::tag('th', 'Результат');
            echo Html::tag('th', 'Средний рейтинг соперников');
            echo Html::tag('th', 'Норма набранных очков');
            echo Html::tag('th', 'Разряд');
            echo Html::tag('th', 'Количество фактически сыгранных партий');
            echo Html::tag('th', 'Пол');
            echo Html::endTag('tr');
            echo Html::endTag('thead');
            echo Html::beginTag('tbody');
            foreach ($tournamentPlayers as $player) {
                echo Html::beginTag('tr');
                echo Html::tag('td', $player->start_number);
                echo Html::tag('td', $player->player_name);
                echo Html::tag('td', $player->rating_start);
                echo Html::tag('td', $player->result);
                echo Html::tag('td', $player->avg_rating_rival);
                echo Html::tag('td', $player->norma);
                echo Html::tag('td', $player->categories);
                echo Html::tag('td', $player->count_games);
                echo Html::tag('td', $player->gender);
                echo Html::endTag('tr');
                $player;
            }
            echo Html::endTag('tbody');
            echo Html::endTag('table');

            echo Html::tag('h2', 'Условия выполнения разрядов');
            echo $this->render('_normaCountTable', array('countRaund' => getCountRaund($tournamentPlayers)));
            echo Html::tag('h2', 'Таблица РШФ');
            echo($ruchessHtmlTable);
        }

        function getCountRaund($tournamentPlayers)
        {
            $countRaund = 0;
            foreach ($tournamentPlayers as $player) {
                if ($countRaund < $player->count_games) {
                    $countRaund = $player->count_games;
                }
            }
            return $countRaund;
        }

        ?>
        <div class="row">
            <div class="col-lg-4">
                <h2>Правила присвоения шахматных разрядов</h2>

                <p>Правила присвоения разрядов по шахматам опубликованы на официальном сайте Министерства спорта
                    Российской Федерации, в разделе спорт высших достижений</p>
                <a href="https://minsport.gov.ru/sport/high-sport/edinaya-vserossiyska/31598/">ЕВСК 2018 - 2021 г</a>

                <p><a class="btn btn-default"
                      href="http://www.minsport.gov.ru/sport/high-sport/edinaya-vserossiyska/5507/">Правила &raquo;</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Рейтинговые турниры РШФ</h2>
                <p>Для просмотра списка шахматных турниров обсчитываемых российской шахматной федерацией, нажмите на
                    кнопку
                    ниже</p>
                <p><a class="btn btn-default" href="http://ratings.ruchess.ru/tournaments">Турниры РШФ &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Расчёт шахматных разрядов</h2>

                <p>Данный сервис находиться на этапе РАЗРАБОТКИ, возможны неточности в расчётах. Если Вы нашли ошибку -
                    прошу написать на электронный адрес.</p>
                <p>Расчёт производится согласно ЕВСК 2018 - 2021 г.</p>
                <p>В выводимых результатах не учитывается ограничение на количество туров, необходимое для присвоения
                    разрядов</p>
                <p>Не обсчитываются шахматные турниры проводимые по круговой системе.</p>
                <p>Есть предложение что добавить/удалить/изменить? - Пиши на электронный адрес.</p>
            </div>
        </div>
    </div>
</div>