<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "players_categories".
 *
 * @property integer $id
 * @property integer $tournament_id
 * @property integer $start_number
 * @property string $player_name
 * @property double $rating_start
 * @property double $result
 * @property double $avg_rating_rival
 * @property double $norma
 * @property string $categories
 * @property integer $count_games
 * @property string $rival_ratings
 * @property string $rival_ids
 * @property integer $html_id
 *
 * @property Tournament $tournament
 */
class PlayersCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'players_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tournament_id', 'player_name'], 'required'],
            [['tournament_id', 'start_number', 'count_games', 'html_id'], 'integer'],
            [['rival_ratings', 'rival_ids'], 'string', 'max' => 500],
            [['rating_start', 'result', 'avg_rating_rival', 'norma'], 'number'],
            [['categories'], 'string', 'max' => 4],
            [['gender'], 'string', 'max' => 1],
            [['player_name'], 'string', 'max' => 280],
            [['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tournament::className(), 'targetAttribute' => ['tournament_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tournament_id' => 'Tournament ID',
            'start_number' => 'Start Number',
            'player_name' => 'Player Name',
            'rating_start' => 'Rating Start',
            'result' => 'Result',
            'avg_rating_rival' => 'Avg Rating Rival',
            'norma' => 'Norma',
            'categories' => 'Categories',
            'count_games' => 'Count Games',
            'rival_ratings' => 'Rival Ratings',
            'rival_ids' => 'Rival Ids',
            'html_id' => 'Html ID',
            'gender'=> 'Gender'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTournament()
    {
        return $this->hasOne(Tournament::className(), ['id' => 'tournament_id']);
    }

    public static function getPlayersWithCategories($tournamentId)
    {
        $a = PlayersCategories::find()
            ->where(['tournament_id' => $tournamentId])
            ->all();
        return $a;
    }

    public static function savePlayerCategories($tournamentPlayers, $tournamentId, $htmlId)
    {
        foreach ($tournamentPlayers as $player) {
            $pc = new PlayersCategories();
            $pc->tournament_id = $tournamentId;
            $pc->html_id = $htmlId;
            $pc->start_number = $player->getNumber();
            $pc->player_name = $player->getFio();
            $pc->rating_start = $player->getRating();
            $pc->result = $player->getResult();
            $pc->avg_rating_rival = $player->getAvgRatingRival();
            $pc->norma = $player->getNorma();
            $pc->categories = $player->getSportCategories();
            $pc->count_games = count($player->getIdsRival());
            $pc->rival_ratings = implode('|', $player->getRatingRival());
            $pc->rival_ids = implode('|', $player->getIdsRival());
            $pc->gender = $player->getGender();
            $pc->save();
        }
    }
}
