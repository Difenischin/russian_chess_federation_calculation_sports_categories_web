<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tournament_url".
 *
 * @property integer $id
 * @property string $url
 * @property string $name
 * @property string $date
 */
class TournamentUrl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tournament_url';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'name'], 'string'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'name' => 'Name',
            'date' => 'Date',
        ];
    }
}
