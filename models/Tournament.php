<?php

namespace app\models;

use Yii;
use app\models\Statistic;
use app\models\PlayersCategories;
use app\models\TournamentHtml;
use app\components\ruchess\Tester;

/**
 * This is the model class for table "tournament".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $drawing_method
 * @property string $region
 * @property string $city
 * @property string $time_control
 * @property string $time_control_type
 * @property string $start_date
 * @property string $end_date
 * @property string $link
 * @property string $chief_arbitr
 * @property string $chief_secretary
 * @property integer $html_id
 *
 * @property PlayersCategories[] $playersCategories
 * @property Statistic[] $statistics
 * @property TournamentHtml $html
 */
class Tournament extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tournament';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'region', 'time_control', 'link', 'chief_arbitr', 'chief_secretary'], 'string'],
            [['start_date', 'end_date'], 'safe'],
            [['html_id'], 'integer'],
            [['url'], 'string', 'max' => 150],
            [['drawing_method', 'time_control_type'], 'string', 'max' => 15],
            [['city'], 'string', 'max' => 85],
            [['html_id'], 'exist', 'skipOnError' => true, 'targetClass' => TournamentHtml::className(), 'targetAttribute' => ['html_id' => 'html_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'drawing_method' => 'Drawing Method',
            'region' => 'Region',
            'city' => 'City',
            'time_control' => 'Time Control',
            'time_control_type' => 'Time Control Type',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'link' => 'Link',
            'chief_arbitr' => 'Chief Arbitr',
            'chief_secretary' => 'Chief Secretary',
            'html_id' => 'Html ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayersCategories()
    {
        return $this->hasMany(PlayersCategories::className(), ['tournament_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatistics()
    {
        return $this->hasMany(Statistic::className(), ['tournament_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHtml()
    {
        return $this->hasOne(TournamentHtml::className(), ['html_id' => 'html_id']);
    }

    public static function isTournamentUrlValide($url = '')
    {
        $pos = strripos($url, 'ratings.ruchess.ru/tournaments/');
        if ($pos == true) {
            $MAX_COUNT_SIMVOL_IN_URL = 55;
            $countSimvolInUrl = strlen($url);
            if($countSimvolInUrl < $MAX_COUNT_SIMVOL_IN_URL){
                return true;
            }
        }
        return false;
    }

    public static function calculateThisTournaments($tournamentUrl)
    {
        /*
 * А расчитывали ли мы разряды для данного турнира( ирл турнира)
 * ишем в таблице турниров такой урл
 * если он есть
 *      вернуть есть ли поле htmlId?
 *
 * усли его нету то возваращаем нет
 * */
        $tournament = Tournament::getTournament_Id_HtmlId_Url_Name_City($tournamentUrl);
        if (count($tournament) > 0) {
            return $tournament;
        }

        return false;

    }

    public static function getTournament_Id_HtmlId_Url_Name_City($tournamentUrl)
    {
        $tournament = Tournament::find()
            ->select('id, html_id, url, name, city')
            ->where(['url' => $tournamentUrl])
            ->one();
        return $tournament;
    }


    public static function getFrontendDateByDB($tournament)
    {
        /*
 * если да тогда выводим информацию из базы
 *      получить в таблице tournament_html поле html_info
 *      получить последнию запись players_categories
 * */
        $result = [];
        $tournamentHtml = TournamentHtml::findOne($tournament->html_id);
        $result['tournamentInfo'] = $tournamentHtml->html_info;
        $result['ruchessHtmlTable'] = $tournamentHtml->html_table;
        $result['tournamentName'] = $tournament->name;
        $result['tournamentPlayers'] = PlayersCategories::getPlayersWithCategories($tournament->id);
        return $result;
    }

    public static function saveTournament($ruchessTournament, $htmlId, $tournamentUrl)
    {
        $t = new Tournament();
        $name =$ruchessTournament->getTournamentName();
        $t->name = $name;
        $t->url = $tournamentUrl;
        $t->html_id = $htmlId;
        $region =$ruchessTournament->getLocation();
        $t->region = $region;
        $city = $ruchessTournament->getLocation();
        $t->city = $city;
        $t->save();
        $id =$t->id;
        return $id;
    }
}
