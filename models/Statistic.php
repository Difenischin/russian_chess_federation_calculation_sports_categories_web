<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statistic".
 *
 * @property integer $id
 * @property integer $session_id
 * @property string $ip
 * @property integer $tournament_id
 * @property string $date
 * @property string $name
 * @property string $tournament_url
 * @property string $city
 *
 * @property Tournament $tournament
 */
class Statistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['session_id', 'tournament_id'], 'integer'],
            [['date'], 'safe'],
            [['name', 'tournament_url'], 'required'],
            [['name'], 'string'],
            [['ip', 'city'], 'string', 'max' => 220],
            [['tournament_url'], 'string', 'max' => 150],
            [['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tournament::className(), 'targetAttribute' => ['tournament_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session_id' => 'Session ID',
            'ip' => 'Ip',
            'tournament_id' => 'Tournament ID',
            'date' => 'Date',
            'name' => 'Name',
            'tournament_url' => 'Url турнира',
            'city' => 'City',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTournament()
    {
        return $this->hasOne(Tournament::className(), ['id' => 'tournament_id']);
    }


    public function saveByParamentrs($city, $name, $tournamentId, $tournamentUrl)
    {
        $this->tournament_id = $tournamentId;
        $this->tournament_url = $tournamentUrl;
        $this->city = $city;
        $this->name = $name;
        $this->session_id = Yii::$app->session->getId();
        $this->ip = Yii::$app->getRequest()->getUserIP();
        $this->save();
    }

    public static function getLastCalculatidTournaments()
    {
        $tournamentUrls = Statistic::find()
            ->select('name, tournament_url, city')
            ->where('(tournament_url like "https://ratings.ruchess.ru/tournaments/%") and (name is not null)')
            ->orderBy(['id' => SORT_DESC])
            ->limit(15)
            ->distinct()
            ->all();
        return $tournamentUrls;
    }
}
