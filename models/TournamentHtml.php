<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tournament_html".
 *
 * @property integer $html_id
 * @property string $download_data
 * @property string $html_info
 * @property string $html_table
 *
 * @property Tournament[] $tournaments
 */
class TournamentHtml extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tournament_html';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['download_data'], 'safe'],
            [['html_info', 'html_table'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'html_id' => 'Html ID',
            'download_data' => 'Download Data',
            'html_info' => 'Html Info',
            'html_table' => 'Html Table',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTournaments()
    {
        return $this->hasMany(Tournament::className(), ['html_id' => 'html_id']);
    }

    public static function getHtmlInfo($htmlId)
    {
        $tournamentHtml = TournamentHtml::findOne($htmlId);
        return $tournamentHtml->html_info;
    }
    public static function saveTournamentHtml($htmlInfo, $htmlTable)
    {
        $t = new TournamentHtml();
        $t->html_info = $htmlInfo;
        $t->html_table = $htmlTable;
        $t->save();
        return $t->html_id;
    }
}
