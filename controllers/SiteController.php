<?php

namespace app\controllers;
//namespace app\components\ruchess;
//namespace app\components\ruchess\Tester;

use app\components\ruchess\SportsCategories;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;

//use app\models\Tester;
//use app\models\Parser;
//use app\models\TournamentUrl;
use app\models\Statistic;
use app\models\PlayersCategories;
use app\models\Tournament;
use app\models\TournamentHtml;
use app\components\ruchess\Tester;
use app\components\ruchess\Tornament;
use app\components\ruchess\RuchessDOMParser;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login'],
                'rules' => [
                    [
                        'actions' => ['logout', 'login'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'login' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        SportsCategories::testSportCategoriesByResultCount();
//        Tester::complexTest();
//        return $this->render('test');
        $model = new Statistic();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $tr = $post['Statistic'];
            $tournamentUrl = $tr['tournament_url'];
            return $this->processingNewUrl($tournamentUrl, $model);
            //return $this->getResultTornamentData($post, $model);
        } else {
            return $this->renderDafaultTornament($model);
        }
    }

    private function getResultTornamentData($post, $model)
    {
        try {
            $tr = $post['Statistic'];
            $tournamentUrl = $tr['tournament_url'];
            return $this->processingNewUrl($tournamentUrl, $model);

        } catch (\Exception $ex) {
            return $this->renderDafaultTornament($model);
        }
    }

    private function processingNewUrl($tournamentUrl, $model)
    {
        if (Tournament::isTournamentUrlValide($tournamentUrl)) {
            $fromFrontend = $this->prozessingCalculationAndSaveTornament($tournamentUrl, $model);
            return $this->renderTornament($fromFrontend, $model);
        } else {
            return $this->renderDafaultTornament($model);
        }
    }

    private function prozessingCalculationAndSaveTornament($tournamentUrl, $model)
    {
        /*
         * А расчитывали ли мы разряды для данного турнира?
         * если да тогда выводим информацию из базы
         * если нет то стягиваем данные из ручесс и сохраняем их у себя
         *
         *
         * */
        //return $this->getNewRuchessTournament($tournamentUrl);
        $tournament = Tournament::calculateThisTournaments($tournamentUrl);
        if ($tournament == false) {
            return $this->getNewRuchessTournament($tournamentUrl, $model);
        } else {
            $model->saveByParamentrs($tournament->city, $tournament->name, $tournament->id, $tournamentUrl);
            $result = Tournament::getFrontendDateByDB($tournament);
            return $result;
        }
    }


    private function getNewRuchessTournament($tournamentUrl, $model)
    {
        /*
         * если нет то стягиваем данные из ручесс и сохраняем их у себя
         *      стягиваем данные из ручесс
         *      сохраняем их у себя
         *          вначале сохраняем tournament_html
         *          потом сохраняем tournament (передавая htmlId)
         *          потом сохраняем players_categories
         *          потом сохраняем statistic
         *
         * */
        $ruchessTournament = $this->getRuchessTournament($tournamentUrl);

        $htmlInfo = $this->getTournamentInfoHtml($ruchessTournament);
        $htmlTable = $this->getTableHtml($ruchessTournament);
        $htmlId = TournamentHtml::saveTournamentHtml($htmlInfo, $htmlTable);

        $tournamentId = Tournament::saveTournament($ruchessTournament, $htmlId, $tournamentUrl);

        $players = $this->getPlayers($ruchessTournament);
        PlayersCategories::savePlayerCategories($players, $tournamentId, $htmlId);

        $model->saveByParamentrs($ruchessTournament->getLocation(), $ruchessTournament->getTournamentName(), $tournamentId, $tournamentUrl);


        return array('ruchessHtmlTable' => $htmlTable,
            'tournamentPlayers' => PlayersCategories::getPlayersWithCategories($tournamentId),
            'tournamentName' => $ruchessTournament->getTournamentName(),
            'tournamentInfo' => $htmlInfo,
        );
    }

    private function getRuchessTournament($tournamentUrl)
    {
        $html = Tester::getDOMbyURL($tournamentUrl);
        $r = new RuchessDOMParser();
        $r->inizialization($html);
        return $r;
    }

    private function getTournamentInfoHtml($ruchessTournament)
    {
        $htmlInfo = $ruchessTournament->getTournamentInfo()->outertext;
        return $htmlInfo;
    }

    private function getTableHtml($ruchessTournament)
    {
        $htmlTable = $ruchessTournament->getTable()->outertext;
        return $htmlTable;
    }


    private function getPlayers($ruchessTournament)
    {
        $players = $ruchessTournament->getPlayers();
        $countRaund = $ruchessTournament->getCountRaund();
        $tournament = new Tornament();
        $tournament->inizialization($players, $countRaund);
        return $tournament->getPlayers();
    }


    private function renderTornament($fromFrontend, $model)
    {
        return $this->render('indexByUrl', [
            'model' => $model,
            'ruchessHtmlTable' => $fromFrontend['ruchessHtmlTable'],
            'tournamentPlayers' => $fromFrontend['tournamentPlayers'],
            'tournamentName' => $fromFrontend['tournamentName'],
            'tournamentInfo' => $fromFrontend['tournamentInfo'],
            'lastTournament' => Statistic::getLastCalculatidTournaments(),
        ]);
    }

    private function renderDafaultTornament($model)
    {
        return $this->render('index', [
            'model' => $model,
            'lastTournament' => Statistic::getLastCalculatidTournaments(),
        ]);
    }

    public function actionView($url)
    {//http://localhost/site.local/www/ratings.ru/basic/web/site/view?url=http://ratings.ruchess.ru/tournaments/11003
        if (Tournament::isTournamentUrlValide($url)) {
            $model = new Statistic();
            $model->tournament_url = $url;
            return $this->getResultTornamentDataByUrl($url, $model);
        }
        return $this->renderDafaultTornament(new Statistic());
    }

    private function getResultTornamentDataByUrl($tournamentUrl, $model)
    {
        try {
            return $this->processingNewUrl($tournamentUrl, $model);

        } catch (\Exception $ex) {
            return $this->renderDafaultTornament($model);
        }
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
