<?php
namespace app\components\ruchess;
class Tornament
{
    private $namber;
    private $countRaund;
    private $players;

    public function inizialization( $players, int $countRaund)
    {
        $this->players = Player::calculeteRatings( $players);
        $this->countRaund = $countRaund;
        $this->setNorma();
        $this->calculeteSportCategories();
    }
    private function calculeteSportCategories()
    {
        foreach ($this->players as $p) {
            $p->calculateSportCategories($this->countRaund);
        }
    }
    private function setNorma()
    {
        foreach ($this->players as $p) {
            $p->setNorma( $this->countRaund);
        }
    }
    public function getPlayers()
    {
        return $this->players;
    }
}