<?php

namespace app\components\ruchess;

use darkdrim\simplehtmldom\SimpleHTMLDom as SHD;

class Parser
{
    public function getDOMbyURL($url)
    {
        $context = $this->getDelaultContext();

        $file = file_get_contents($url, false, $context);
        $html = SHD::str_get_html($file);

        return $html;
    }

    public function getDOMbyFile($url)
    {
        $file = file_get_contents($url);
        $html = SHD::str_get_html($file);
        return $html;
    }

    private function getDelaultContext()
    {
        $context = stream_context_create(array(
            'http' => array(
                'header' => array('User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201'),
            ),));
        return $context;
    }
}

class RuchessDOMParser
{
    private $html;
    private $table;
    private $tournamentName;
    private $tournamentInfo;
    private $drawingMethod;
    private $location;


    const DRAWING_METHOD = 'Метод жеребьёвки: ';
    const DWAWING_METOD_NUMBER = 0;

    const LOCATION = 'Место проведения:';
    const LOCATION_NUMBER = 2;


    public function inizialization($html)
    {
        $this->setHtml($html);
        $this->setTable();
        $this->setTournamentName();
        $this->setTournamentInfo();
        $this->setDrawingMethod();
        $this->setLocation();
    }

    public function setHtml($html)
    {
        $this->html = $html;
    }

    private function setTable()
    {
        $out = new TableParser();
        $out->inizialization($this->html);
        $this->table = $out;
    }

    private function setTournamentName()
    {
        $this->tournamentName = $this->html->find('h1')[0]->plaintext;
    }

    private function setTournamentInfo()
    {
        $this->tournamentInfo = $this->html->find("div[class='panel panel-default']")[0];
    }

    private function setDrawingMethod()
    {
        $li = $this->getTextElementInTournamentInfo(self::DWAWING_METOD_NUMBER);
        $liClearStrong = str_replace(self::DRAWING_METHOD, '', $li);
        $this->drawingMethod = preg_replace('/\s/', '', $liClearStrong);
    }

    private function getTextElementInTournamentInfo($selector)
    {
        return $this->tournamentInfo->find('li[class="list-group-item"]')[$selector]->plaintext;
    }

    public function getDrawingMethod()
    {
        return $this->drawingMethod;
    }

    private function setLocation()
    {
        $li = $this->getTextElementInTournamentInfo(self::LOCATION_NUMBER);
        $liClearStrong = str_replace(self::LOCATION, '', $li);
        $this->location = trim(preg_replace('/\ss/', '', $liClearStrong));
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getCountRaund()
    {
        return $this->table->getCountRaund();
    }

    public function getPlayers()
    {
        return $this->table->getPlayers();
    }

    public function getTable()
    {
        return $this->table->getTable();
    }

    public function getTournamentName()
    {
        return $this->tournamentName;
    }

    public function getTournamentInfo()
    {
        return $this->tournamentInfo;
    }
}

class TableParser
{
    private $table;
    private $HTML_TEG_ROW_IN_TABLE = 'tr';
    private $URL_FOR_GET_GENDER = 'http://ratings.ruchess.ru';

    private $preloadPlayers = [];

    public function inizialization($html)
    {
        $out = DOMParser::findInDoM($html, 'table');
        $this->table = $out[0];
        $this->setPreloadPlayers();
    }

    private function setPreloadPlayers()
    {
        $file = __DIR__;
        if (($handle = fopen($file . '/smaster_rapid.csv', "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $cellsCount = count($data);
                $cells = [];
                for ($c = 0; $c < $cellsCount; $c++) {
                    $cells[] = $data[$c];
                }
                $this->preloadPlayers[] = $cells;
            }
            fclose($handle);
        }
        /* $file = __DIR__;
         $csvFile = file($file . '/smaster_rapid.csv');
         foreach ($csvFile as $line) {
             $this->preloadPlayers[] = str_getcsv($line);
         }*/
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getCountRaund()
    {
        $getNameСolumnString = $this->getNameСolumnString();

        $rows = DOMParser::findInDoM($getNameСolumnString, 'th');
        $defaultColumsCount = 9;//№  Фед  Имя участника  Rнач  Очки  Место  Rср Rнов  Нор
        $result = count($rows) - $defaultColumsCount;

        return $result;
    }

    private function getNameСolumnString()
    {
        $header = 0;
        return $this->getRowTable($header);
    }

    private function getRowTable($namberString)
    {
        return $this->table->find($this->HTML_TEG_ROW_IN_TABLE, $namberString);
    }


    public function getPlayers()
    {
        $players = [];
        $countPlayers = $this->getCountPlayers();
        for ($lineInTable = 1; $lineInTable <= $countPlayers; $lineInTable++) {
            $rowTable = $this->getRowTable($lineInTable);
            $players[] = $this->getPlayer($rowTable);
        }
        return $players;
    }

    private function getCountPlayers()
    {
        $countRowsInTable = count($this->table->find($this->HTML_TEG_ROW_IN_TABLE));
        $headerRows = 1;
        return ($countRowsInTable - $headerRows);
    }

    private function getPlayer($rowTable)
    {
        $cells = DOMParser::findInDoM($rowTable, 'td');
        $player = new Player();
        $player->setNumber($cells[0]->plaintext);
        $player->setFederation($cells[1]->plaintext);
        $player->setFio($cells[2]->plaintext);
        $player->setRating($cells[3]->plaintext);
        $player->setIdsRival($this->getIdsRival($cells));
        $player->setResult($this->getResult($cells));
        $player->setGender($this->loadAndGetGendder($cells[2]));
        return $player;
    }

    private function loadAndGetGendder($userUrl)
    {
        try {
            $urlUserIdHref = $userUrl->children[0]->href;
            $preloadGender = $this->getPreloadGender($urlUserIdHref);
            if ($preloadGender === 'no found') {
                $loadedGender = $this->loadGender($urlUserIdHref);
                $genderChar = mb_substr($loadedGender, -1, 1, 'UTF-8');
                return $genderChar;
            }
            return $preloadGender;
        } catch (\Exception $ex) {
            return 'М';
        }
    }

    private function getPreloadGender($urlUserIdHref)
    {
        $userId = (int)$this->getIdUserByUrl($urlUserIdHref);
        $players = $this->preloadPlayers;
        for ($i = 0; $i < count($players); $i++) {
            $id = (int)($players[$i][0]);
            if ($id === $userId) {
                $b = $players[$i][7];
                $gender = $this->getPreloadGenderForFind($b);
                return $gender;
            }
        }
        return 'no found';
    }

    private function getIdUserByUrl($userUrl)
    {
        return str_ireplace('/people/', '', $userUrl);
    }

    private function getPreloadGenderForFind($preloadPlayer)
    {
        if ($preloadPlayer === 'w') {
            return 'Ж';
        }
        return 'М';
    }

    private function loadGender($urlUserIdHref)
    {
        $urlForParse = $this->URL_FOR_GET_GENDER . $urlUserIdHref;
        $parser = new Parser();
        $DOM = $parser->getDOMbyURL($urlForParse);
        $cells = DOMParser::findInDoM($DOM, 'li[class]')[2];
        return $cells->plaintext;
    }


    private function getIdsRival($cells)
    {
        $playerParser = new PlayerParser();
        $playerParser->inizialization($cells);
        return $playerParser->getIdsRival();
    }

    private function getResult($cells)
    {
        $playerParser = new PlayerParser();
        $playerParser->inizialization($cells);
        return $playerParser->getResult();
    }
}

class DOMParser
{
    public static function findInDoM($html, $elementDOM)
    {
        $out = [];
        foreach ($html->find($elementDOM) as $element)
            $out[] = $element;
        return $out;
    }
}

class PlayerParser
{
    private $rowTable;
    private $COUNT_MAX_SIMVOL_IN_ROW = 8;

    public function inizialization($rowTable)
    {
        $this->rowTable = $rowTable;
    }

    public function getIdsRival()
    {
        $idsRival = [];
        $countCell = count($this->rowTable);

        for ($cellNamber = 0; $cellNamber < $countCell; $cellNamber++) {
            $cell = $this->rowTable[$cellNamber]->plaintext;
            if ($this->isIdRival($cell)) {
                $idsRival[] = $this->getIdRival($cell);
            }
        }

        return $idsRival;
    }

    private function isIdRival($cell)
    {
        $countSimvol = strlen($cell);
        $blak = strripos($cell, 'ч');
        if ($blak == true && $countSimvol < $this->COUNT_MAX_SIMVOL_IN_ROW) {
            return true;
        }
        $white = strripos($cell, 'б');
        if ($white == true && $countSimvol < $this->COUNT_MAX_SIMVOL_IN_ROW) {
            return true;
        }
        return false;
    }

    private function getIdRival($cell)
    {
        if (stristr($cell, 'б', true) != false)
            return stristr($cell, 'б', true);

        if (stristr($cell, 'ч', true) != false)
            return stristr($cell, 'ч', true);

        return false;
    }


    public function getResult()
    {
        $result = '';
        $countCell = count($this->rowTable);

        for ($cellNamber = 0; $cellNamber < $countCell; $cellNamber++) {
            $cell = $this->rowTable[$cellNamber]->plaintext;
            if ($this->isIdResult($cell)) {
                $result = $cell;
            }
        }
        return $result;
    }

    private function isIdResult($cell)
    {
        $pos = strripos($cell, '.');
        $countSimvol = strlen($cell);

        if ($pos == true && $countSimvol < $this->COUNT_MAX_SIMVOL_IN_ROW) {
            return true;
        }
        return false;
    }
}