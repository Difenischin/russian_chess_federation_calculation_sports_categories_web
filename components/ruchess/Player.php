<?php

namespace app\components\ruchess;
class Player
{
    private $namber;
    private $federation;
    private $fio;
    private $rating;
    private $sportCategories;
    private $result;
    private $avgRatingRival;
    private $idsRival;
    private $ratingRival;
    private $resultInActualGames;//количество набранных очков без +/-
    private $norma;
    private $gender;


    public function setNumber($number)
    {
        $this->namber = $number;
    }

    public function setFederation($federation)
    {
        $this->federation = $federation;
    }

    public function setFio($fio)
    {
        $this->fio = $fio;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    public function setIdsRival($idsRival)
    {
        $this->idsRival = $idsRival;
    }

    public function setResult($result)
    {
        $this->result = $result;
    }

    public function setRatingRival($ratingRival)
    {
        $this->ratingRival = $ratingRival;
    }

    public function setAvgRatingRival($avgRatingRival)
    {
        $this->avgRatingRival = $avgRatingRival;
    }

    public function setGender($genderChar)
    {
        $this->gender = $genderChar;
    }

    public function getNumber()
    {
        return $this->namber;
    }

    public function getFederation()
    {
        return $this->federation;
    }

    public function getFio()
    {
        return $this->fio;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function getIdsRival()
    {
        return $this->idsRival;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getRatingRival()
    {
        return $this->ratingRival;
    }

    public function getAvgRatingRival()
    {
        return $this->avgRatingRival;
    }

    public function getSportCategories()
    {
        return $this->sportCategories;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public static function getPlayerByNamber($players, $namber)
    {
        foreach ($players as $player) {
            if ($player->getNumber() == $namber) {
                return $player;
            }
        }
        return false;
    }

    public static function calculeteRatings($players)
    {//тут есть потенциальная уязвимость в обращении к игроку
        $countPlayers = count($players);
        for ($i = 0; $i < $countPlayers; $i++) {
            $rivalIds = $players[$i]->getIdsRival();
            $ratingRival = self::calculateAndGetRatingRival($players, $rivalIds);
            $players[$i]->setRatingRival($ratingRival);
            $roundAvgRatingRival = self::getRoundAvgRatingRival($players[$i]->getRatingRival());
            $players[$i]->setAvgRatingRival($roundAvgRatingRival);
        }
        return $players;
    }

    private static function calculateAndGetRatingRival($players, $rivalIds)
    {//тут есть потенциальная уязвимость в обращении к игроку
        $result = [];
        foreach ($rivalIds as $idRival) {
            $result[] = $players[$idRival - 1]->getRating();
        }
        return $result;
    }

    public static function getRoundAvgRatingRival($ratingRival)
    {
        if (isset($ratingRival) && count($ratingRival) > 0) {
            $rating = 0;
            foreach ($ratingRival as $key) {
                $rating += $key;
            }
            $avgRatingRival = $rating / count($ratingRival);
            return round($avgRatingRival, 4);
        }

        return 0;
    }

    public function calculateSportCategories($countTournamentRounds)
    {
        $a = SportsCategories::getSportCategoriesByResultCount($this->getAvgRatingRival(), $this->getResult(), $countTournamentRounds, $this->getGender());
        $this->sportCategories = $a;
    }

    public function setNorma($countTournamentRounds)
    {
        $resultInActualGames = $this->getResult();
        if ((float)$this->getResult() > count($this->getIdsRival())) {
            $resultInActualGames = count($this->getIdsRival());
        }
        $norma = (float)$resultInActualGames / $countTournamentRounds;
        $this->norma = round($norma, 4);
    }

    public function getNorma()
    {
        return $this->norma;
    }

}
