<?php

namespace app\components\ruchess;

class Tester
{
    public static function complexTest()
    {
        $url = 'http://ratings.ruchess.ru/tournaments/11003';
//        $url = 'http://ratings.ruchess.ru/tournaments/55890';
        //ini_set('memory_limit', '-1');
        $DOM = Tester::getDOMbyURL($url);
        //$DOM = Tester::getDOMbyFile("test.html");
        //$out = Tester::findInDoM( $DOM);
        // url который ломает систему http://ratings.ruchess.ru/tournaments/11613
        //http://ratings.ruchess.ru/tournaments/13465 - очень долгий
        //http://ratings.ruchess.ru/tournaments/15668
        //http://ratings.ruchess.ru/tournaments/17158 - тут неправильно считывался город
        Tester::testCountRound($DOM);
        Tester::testTournamentName($DOM);
        Tester::testGetPlayers($DOM);
        Tester::testDrawingMethod($DOM);
        self::testLocation($DOM);
        Tester::calculationAvgRating($DOM);
        //print_r(Tester::getPlayers( $DOM));
    }

    public function testCountRound($DOM)
    {
        if (Tester::getCountRaund($DOM) == 7) {
            print_r(' CountRaundTest = true; ');
        } else {
            print_r(' CountRaundTest = false; ');
        }
    }

    public function testGetPlayers($DOM)
    {
        $players = Tester::getPlayers($DOM);
        $idsRival = $players[0]->getIdsRival();
        if ($idsRival[0] == 14
            && $idsRival[1] == 8
            && $idsRival[2] == 4
            && $idsRival[3] == 27
            && $idsRival[4] == 2
            && $idsRival[5] == 23
            && $idsRival[6] == 22
        ) {
            print_r(' testGetPlayers = true; ');
        } //1  RUS Исаев Антон 2184    14ч1    8б1 4ч1 27б½    2ч1 23б1    22б1    6.5
        else {
            print_r(' testGetPlayers = false; ');
        }
        if ($players[0]->getResult() == 6.5) {
            print_r(' testGetPlayers result = true; ');
        } else {
            print_r(' testGetPlayers result = false; ');
        }
    }

    public function testDrawingMethod($DOM)
    {
        if (Tester::getDrawingMethod($DOM) == 'Швейцарская') {
            print_r(' testDrawingMethod = true; ');
        } else {
            print_r(' testDrawingMethod = false; ');
        }
    }

    private function getDrawingMethod($html)
    {
        $r = new RuchessDOMParser();
        $r->inizialization($html);
        return $r->getDrawingMethod();
    }

    private function testLocation($DOM)
    {
        if (Tester::getLocation($DOM) == 'Томск, Томская область') {
            print_r(' testLocation = true; ');
        } else {
            print_r(' testLocation = false; ');
        }
    }

    private function getLocation($html)
    {
        $r = new RuchessDOMParser();
        $r->inizialization($html);
        return $r->getLocation();
    }

    public function testTournamentName($DOM)
    {
        $r = new RuchessDOMParser();
        $r->inizialization($DOM);
        if ($r->getTournamentName() == 'Мемориал Петросяна 2017') {
            print_r(' testTournamentName = true; ');
        } else {
            print_r(' testTournamentName = false; ');
            print_r($r->getTournamentName());
        }
    }

    public static function getDOMbyURL($url = 'http://ratings.ruchess.ru/tournaments/11003')
    {
        $parser = new Parser();
        $out = $parser->getDOMbyURL($url);
        return $out;
    }

    public static function getDOMbyFile($url = 'http://ratings.ruchess.ru/tournaments/11003')
    {
        $parser = new Parser();
        $out = $parser->getDOMbyFile($url);
        return $out;
    }

    public static function getCountRaund($html)
    {
        $r = new RuchessDOMParser();
        $r->inizialization($html);
        return $r->getCountRaund();
    }

    public static function getPlayers($html)
    {
        $r = new RuchessDOMParser();
        $r->inizialization($html);
        return $r->getPlayers();
    }

    public static function getTable($html)
    {
        $r = new RuchessDOMParser();
        $r->inizialization($html);
        return $r->getTable();
    }

    public static function calculationAvgRating($DOM)
    {
        $a = Tester::getCalculationAvgRating(Tester::getPlayers($DOM), Tester::getCountRaund($DOM));
        Tester::setRatingRivalsTest($a);
        Tester::setRatingSportCategories($a);
        print_r(Tester::getCalculationAvgRating(Tester::getPlayers($DOM), Tester::getCountRaund($DOM)));

    }

    public static function getCalculationAvgRating($players, $countRaund)
    {
        $r = new Tornament();
        $r->inizialization($players, $countRaund);
        return $r->getPlayers();
    }

    public static function setRatingRivalsTest($players)
    {
        $ratingRival = $players[0]->getRatingRival();
        if ($ratingRival[0] == 1000
            && $ratingRival[1] == 1052
            && $ratingRival[2] == 1744
            && $ratingRival[3] == 1961
            && $ratingRival[4] == 2013
            && $ratingRival[5] == 1000
            && $ratingRival[6] == 1000
        ) {
            print_r(' setRatingRivalsTest = true; ');
        } //1  RUS Исаев Антон 2184    14ч1    8б1 4ч1 27б½    2ч1 23б1    22б1    6.5
        else {
            print_r(' setRatingRivalsTest = false; ');
        }
        if ($players[0]->getAvgRatingRival() == 1395.7143) {//1258.2857142857) {
            print_r(' avgRatingRival = true; ');
        } else {
            print_r(' avgRatingRival = false; ');
        }
    }

    public static function setRatingSportCategories($players)
    {
        $sportCategories = $players[0]->getSportCategories();
        if ($sportCategories == '3') {
            print_r(' SportCategories = true; ');
        } else {
            print_r(' SportCategories = false; ');
        }
    }

    public static function getDataFromFrontend($url = '')
    {
        $html = Tester::getDOMbyURL($url);

        $r = new RuchessDOMParser();
        $r->inizialization($html);

        $domRuchessUrl = $r->getTable();
        $players = $r->getPlayers();
        $countRaund = $r->getCountRaund();

        $tournament = new Tornament();
        $tournament->inizialization($players, $countRaund);

        return array('domRuchessUrl' => $domRuchessUrl,
            'tournamentPlayers' => $tournament->getPlayers(),
            'tournamentName' => $r->getTournamentName(),
            'tournamentInfo' => $r->getTournamentInfo(),
        );
    }
}