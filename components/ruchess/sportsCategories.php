<?php

namespace app\components\ruchess;
class SportsCategories
{
    /*
    11.2.  Норма: % набранных очков к количеству максимально возможных очков в фактически игранных партиях выражаемая в количестве очков вычисляется по формуле: А= (ВхС)/100, где:
А - количество очков,                                                   
В - число, указанное в пункте 11.1 настоящих иных условий соответствует проценту набранных очков от максимального количества очков, которые возможно было набрать в фактически игранных партиях,                                                    
С - количество максимально возможных очков в фактически игранных партиях в проведенном спортивном соревновании.                                                 
12. Если норма спортивного разряда в спортивном соревновании по круговой или швейцарской системе оказывается дробной, то она округляется до ближайшего полуочка.                                                    
*/
    public static function getSportCategories(int $avgRating, $result)
    {
        /*если результат равен нужной норме тогда
        венруть нужную норму:
        */
        if ($result >= 0.75) {
            if ($avgRating >= 1901 && $avgRating <= 1925) {
                return 'КМС';
            }
            if ($avgRating >= 1701 && $avgRating <= 1725) {
                return '1';
            }
            if ($avgRating >= 1501 && $avgRating <= 1525) {
                return '2';
            }
            if ($avgRating >= 1301 && $avgRating <= 1325) {
                return '3';
            }
            if ($avgRating >= 1201 && $avgRating <= 1209) {
                return '1ю';
            }
            if ($avgRating >= 1101 && $avgRating <= 1109) {
                return '2ю';
            }
            self::getSportCategories($avgRating, $result - 0.05);
        }
        if ($result >= 0.70) {
            if ($avgRating >= 1926 && $avgRating <= 1950) {
                return 'КМС';
            }
            if ($avgRating >= 1726 && $avgRating <= 1750) {
                return '1';
            }
            if ($avgRating >= 1526 && $avgRating <= 1550) {
                return '2';
            }
            if ($avgRating >= 1326 && $avgRating <= 1350) {
                return '3';
            }
            if ($avgRating >= 1210 && $avgRating <= 1222) {
                return '1ю';
            }
            if ($avgRating >= 1110 && $avgRating <= 1122) {
                return '2ю';
            }
            self::getSportCategories($avgRating, $result - 0.05);
        }
        if ($result >= 0.65) {
            if ($avgRating >= 1951 && $avgRating <= 1975) {
                return 'КМС';
            }
            if ($avgRating >= 1751 && $avgRating <= 1775) {
                return '1';
            }
            if ($avgRating >= 1551 && $avgRating <= 1575) {
                return '2';
            }
            if ($avgRating >= 1351 && $avgRating <= 1375) {
                return '3';
            }
            if ($avgRating >= 1223 && $avgRating <= 1235) {
                return '1ю';
            }
            if ($avgRating >= 1123 && $avgRating <= 1135) {
                return '2ю';
            }
            self::getSportCategories($avgRating, $result - 0.05);
        }
        if ($result >= 0.60) {
            if ($avgRating >= 1976 && $avgRating <= 2000) {
                return 'КМС';
            }
            if ($avgRating >= 1776 && $avgRating <= 1800) {
                return '1';
            }
            if ($avgRating >= 1576 && $avgRating <= 1600) {
                return '2';
            }
            if ($avgRating >= 1376 && $avgRating <= 1400) {
                return '3';
            }
            if ($avgRating >= 1236 && $avgRating <= 1248) {
                return '1ю';
            }
            if ($avgRating >= 1136 && $avgRating <= 1148) {
                return '2ю';
            }
            if ($avgRating >= 1000 && $avgRating <= 1135) {
                return '3ю';
            }
            self::getSportCategories($avgRating, $result - 0.05);

        }
        if ($result >= 0.55) {
            if ($avgRating >= 2001 && $avgRating <= 2025) {
                return 'КМС';
            }
            if ($avgRating >= 1801 && $avgRating <= 1825) {
                return '1';
            }
            if ($avgRating >= 1601 && $avgRating <= 1625) {
                return '2';
            }
            if ($avgRating >= 1401 && $avgRating <= 1425) {
                return '3';
            }
            if ($avgRating >= 1249 && $avgRating <= 1261) {
                return '1ю';
            }
            if ($avgRating >= 1149 && $avgRating <= 1161) {
                return '2ю';
            }
            if ($avgRating >= 1001 && $avgRating <= 1025) {
                return '3ю';
            }
            self::getSportCategories($avgRating, $result - 0.05);
        }
        if ($result >= 0.50) {
            if ($avgRating >= 2026 && $avgRating <= 2050) {
                return 'КМС';
            }
            if ($avgRating >= 1826 && $avgRating <= 1850) {
                return '1';
            }
            if ($avgRating >= 1626 && $avgRating <= 1650) {
                return '2';
            }
            if ($avgRating >= 1426 && $avgRating <= 1450) {
                return '3';
            }
            if ($avgRating >= 1262 && $avgRating <= 1274) {
                return '1ю';
            }
            if ($avgRating >= 1162 && $avgRating <= 1174) {
                return '2ю';
            }
            if ($avgRating >= 1026 && $avgRating <= 1050) {
                return '3ю';
            }
            self::getSportCategories($avgRating, $result - 0.05);
        }
        if ($result >= 0.45) {
            if ($avgRating >= 2051 && $avgRating <= 2075) {
                return 'КМС';
            }
            if ($avgRating >= 1851 && $avgRating <= 1875) {
                return '1';
            }
            if ($avgRating >= 1651 && $avgRating <= 1675) {
                return '2';
            }
            if ($avgRating >= 1451 && $avgRating <= 1475) {
                return '3';
            }
            if ($avgRating >= 1275 && $avgRating <= 1287) {
                return '1ю';
            }
            if ($avgRating >= 1175 && $avgRating <= 1187) {
                return '2ю';
            }
            if ($avgRating >= 1051 && $avgRating <= 1075) {
                return '3ю';
            }
            self::getSportCategories($avgRating, $result - 0.05);
        }
        if ($result >= 0.40) {
            if ($avgRating >= 2076 && $avgRating <= 2100) {
                return 'КМС';
            }
            if ($avgRating >= 1876 && $avgRating <= 1900) {
                return '1';
            }
            if ($avgRating >= 1676 && $avgRating <= 1700) {
                return '2';
            }
            if ($avgRating >= 1476 && $avgRating <= 1500) {
                return '3';
            }
            if ($avgRating >= 1288 && $avgRating <= 1300) {
                return '1ю';
            }
            if ($avgRating >= 1188 && $avgRating <= 1200) {
                return '2ю';
            }
            if ($avgRating >= 1076 && $avgRating <= 1100) {
                return '3ю';
            }
            self::getSportCategories($avgRating, $result - 0.05);
        }
        if ($result >= 0.35) {
            if ($avgRating > 2100) {
                return 'КМС';
            }
            if ($avgRating > 1900) {
                return '1';
            }
            if ($avgRating > 1700) {
                return '2';
            }
            if ($avgRating > 1500) {
                return '3';
            }
            if ($avgRating > 1300) {
                return '1ю';
            }
            if ($avgRating > 1200) {
                return '2ю';
            }
            if ($avgRating > 1100) {
                return '3ю';
            }
        }
        return '';
    }

    public static function getSportCategoriesByResultCount(float $avgRating, $resultCount, $countTournamentRounds, $gender = 'М')
    {
        $avgRating = round($avgRating);
        $genderCorrector = 0;
        if ($gender === 'Ж') {
            $genderCorrector = 100;
        }

        if ($resultCount >= self::getNormaPoints(0.75, $countTournamentRounds)) {
            if ($avgRating >= (1901 - $genderCorrector) && $avgRating <= (1925 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating >= (1701 - $genderCorrector) && $avgRating <= (1725 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating >= (1501 - $genderCorrector) && $avgRating <= (1525 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating >= (1301 - $genderCorrector) && $avgRating <= (1325 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating >= 1151 && $avgRating <= 1156) {
                return '1ю';
            }
            if ($avgRating >= 1101 && $avgRating <= 1106) {
                return '2ю';
            }
            $currentNorma = self::getNormaPoints(0.75, $countTournamentRounds);
            $nextNorma = self::getNormaPoints(0.70, $countTournamentRounds);
            if ($currentNorma === $nextNorma) {
                self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            } else {
                return self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            }
        }
        if ($resultCount >= self::getNormaPoints(0.70, $countTournamentRounds)) {
            if ($avgRating >= (1926 - $genderCorrector) && $avgRating <= (1950 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating >= (1726 - $genderCorrector) && $avgRating <= (1750 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating >= (1526 - $genderCorrector) && $avgRating <= (1550 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating >= (1326 - $genderCorrector) && $avgRating <= (1350 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating >= 1157 && $avgRating <= 1162) {
                return '1ю';
            }
            if ($avgRating >= 1107 && $avgRating <= 1112) {
                return '2ю';
            }
            $currentNorma = self::getNormaPoints(0.70, $countTournamentRounds);
            $nextNorma = self::getNormaPoints(0.65, $countTournamentRounds);
            if ($currentNorma === $nextNorma) {
                self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            } else {
                return self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            }
        }
        if ($resultCount >= self::getNormaPoints(0.65, $countTournamentRounds)) {
            if ($avgRating >= (1951 - $genderCorrector) && $avgRating <= (1975 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating >= (1751 - $genderCorrector) && $avgRating <= (1775 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating >= (1551 - $genderCorrector) && $avgRating <= (1575 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating >= (1351 - $genderCorrector) && $avgRating <= (1375 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating >= 1163 && $avgRating <= 1168) {
                return '1ю';
            }
            if ($avgRating >= 1113 && $avgRating <= 1118) {
                return '2ю';
            }
            $currentNorma = self::getNormaPoints(0.65, $countTournamentRounds);
            $nextNorma = self::getNormaPoints(0.6, $countTournamentRounds);
            if ($currentNorma === $nextNorma) {
                self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            } else {
                return self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            }
        }
        if ($resultCount >= self::getNormaPoints(0.60, $countTournamentRounds)) {
            if ($avgRating >= (1976 - $genderCorrector) && $avgRating <= (2000 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating >= (1776 - $genderCorrector) && $avgRating <= (1800 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating >= (1576 - $genderCorrector) && $avgRating <= (1600 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating >= (1376 - $genderCorrector) && $avgRating <= (1400 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating >= 1169 && $avgRating <= 1174) {
                return '1ю';
            }
            if ($avgRating >= 1119 && $avgRating <= 1124) {
                return '2ю';
            }
            if ($avgRating === 1000) {
                return '3ю';
            }
            $currentNorma = self::getNormaPoints(0.6, $countTournamentRounds);
            $nextNorma = self::getNormaPoints(0.55, $countTournamentRounds);
            if ($currentNorma === $nextNorma) {
                self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            } else {
                return self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            }
        }
        if ($resultCount >= self::getNormaPoints(0.55, $countTournamentRounds)) {
            if ($avgRating >= (2001 - $genderCorrector) && $avgRating <= (2025 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating >= (1801 - $genderCorrector) && $avgRating <= (1825 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating >= (1601 - $genderCorrector) && $avgRating <= (1625 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating >= (1401 - $genderCorrector) && $avgRating <= (1425 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating >= 1175 && $avgRating <= 1180) {
                return '1ю';
            }
            if ($avgRating >= 1125 && $avgRating <= 1130) {
                return '2ю';
            }
            if ($avgRating >= 1001 && $avgRating <= 1025) {
                return '3ю';
            }
            $currentNorma = self::getNormaPoints(0.55, $countTournamentRounds);
            $nextNorma = self::getNormaPoints(0.5, $countTournamentRounds);
            if ($currentNorma === $nextNorma) {
                self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            } else {
                return self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            }
        }
        if ($resultCount >= self::getNormaPoints(0.50, $countTournamentRounds)) {
            if ($avgRating >= (2026 - $genderCorrector) && $avgRating <= (2050 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating >= (1826 - $genderCorrector) && $avgRating <= (1850 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating >= (1626 - $genderCorrector) && $avgRating <= (1650 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating >= (1426 - $genderCorrector) && $avgRating <= (1450 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating >= 1181 && $avgRating <= 1185) {
                return '1ю';
            }
            if ($avgRating >= 1131 && $avgRating <= 1135) {
                return '2ю';
            }
            if ($avgRating >= 1026 && $avgRating <= 1050) {
                return '3ю';
            }
            $currentNorma = self::getNormaPoints(0.5, $countTournamentRounds);
            $nextNorma = self::getNormaPoints(0.45, $countTournamentRounds);
            if ($currentNorma === $nextNorma) {
                self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            } else {
                return self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            }
        }
        if ($resultCount >= self::getNormaPoints(0.45, $countTournamentRounds)) {
            if ($avgRating >= (2051 - $genderCorrector) && $avgRating <= (2075 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating >= (1851 - $genderCorrector) && $avgRating <= (1875 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating >= (1651 - $genderCorrector) && $avgRating <= (1675 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating >= (1451 - $genderCorrector) && $avgRating <= (1475 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating >= 1186 && $avgRating <= 1190) {
                return '1ю';
            }
            if ($avgRating >= 1136 && $avgRating <= 1140) {
                return '2ю';
            }
            if ($avgRating >= 1051 && $avgRating <= 1075) {
                return '3ю';
            }
            $currentNorma = self::getNormaPoints(0.45, $countTournamentRounds);
            $nextNorma = self::getNormaPoints(0.4, $countTournamentRounds);
            if ($currentNorma === $nextNorma) {
                self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            } else {
                return self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            }
        }
        if ($resultCount >= self::getNormaPoints(0.40, $countTournamentRounds)) {
            if ($avgRating >= (2076 - $genderCorrector) && $avgRating <= (2100 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating >= (1876 - $genderCorrector) && $avgRating <= (1900 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating >= (1676 - $genderCorrector) && $avgRating <= (1700 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating >= (1476 - $genderCorrector) && $avgRating <= (1500 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating >= 1191 && $avgRating <= 1200) {
                return '1ю';
            }
            if ($avgRating >= 1141 && $avgRating <= 1150) {
                return '2ю';
            }
            if ($avgRating >= 1076 && $avgRating <= 1100) {
                return '3ю';
            }
            $currentNorma = self::getNormaPoints(0.4, $countTournamentRounds);
            $nextNorma = self::getNormaPoints(0.35, $countTournamentRounds);
            if ($currentNorma === $nextNorma) {
                self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            } else {
                return self::getSportCategoriesByResultCount($avgRating, $resultCount - 0.5, $countTournamentRounds, $gender);
            }
        }
        if ($resultCount >= self::getNormaPoints(0.35, $countTournamentRounds)) {
            if ($avgRating > (2100 - $genderCorrector)) {
                return 'КМС';
            }
            if ($avgRating > (1900 - $genderCorrector)) {
                return '1';
            }
            if ($avgRating > (1700 - $genderCorrector)) {
                return '2';
            }
            if ($avgRating > (1500 - $genderCorrector)) {
                return '3';
            }
            if ($avgRating > 1200) {
                return '1ю';
            }
            if ($avgRating > 1150) {
                return '2ю';
            }
            if ($avgRating > 1100) {
                return '3ю';
            }
        }
        return '';
    }

    public static function getNormaPoints($percent, $countGames)
    {
        $noRoundResult = (float)$countGames * $percent;
        $result = self::getRoundingRoundResultToNearestHalfPoint($noRoundResult);
        return $result;
    }

    private static function getRoundingRoundResultToNearestHalfPoint($noRoundResult)
    {
        $number = round($noRoundResult, 1);
        $fractionalPartForInt = self::getFractionalPart($number);
        $result = $number - (float)$fractionalPartForInt / 10;
        $result += self::getResultToNearestHalfPoint((int)$fractionalPartForInt);
        return $result;
    }

    private static function getFractionalPart($number)
    {
        return substr($number, strpos($number, '.') + 1);
    }

    private static function getResultToNearestHalfPoint(int $fractionalPartToInt)
    {//зделанно именно так, из -за неприятных побочных эффектов с приведением типов http://php.net/manual/ru/language.types.integer.php#language.types.integer.casting
        $result = 0;
        if ($fractionalPartToInt < 3) {
            $result += 0;
        }
        if (3 <= $fractionalPartToInt && $fractionalPartToInt <= 7) {
            $result += 0.5;
        }
        if (7 < $fractionalPartToInt) {
            ++$result;
        }
        return $result;
    }


    public static function testSportCategoriesByResultCount()
    {
        echo '<br> testSportCategoriesByResultCount start';
        if (self::getSportCategoriesByResultCount(1826, 5, 7, 'Ж') !== 'КМС') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1850, 5, 7, 'Ж') !== 'КМС') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1926, 5, 7, 'Ж') !== 'КМС') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1526, 4, 7, 'Ж') !== '2') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1251, 4.5, 7, 'Ж') !== '3') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1251, 4.5, 7, 'Ж') !== '3') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1751, 4.5, 7, 'М') !== '1') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1576, 5, 7, 'Ж') !== '2') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1601, 2.5, 7, 'Ж') !== '2') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1513.71, 5, 7, 'Ж') !== '2') {
            throw new Exception('Непраивльно считаются разряды');
        }
        if (self::getSportCategoriesByResultCount(1553.71, 4, 7, 'Ж') !== '2') {
            throw new Exception('Непраивльно считаются разряды');
        }

        echo '<br> testSportCategoriesByResultCount end';

        echo '<br> testSportCategoriesByResultCount start2';

        if (self::getSportCategoriesByResultCount(1122.88, 6, 8, 'Ж') !== '2ю') {
            throw new Exception('Непраивльно считаются разряды');
        }

        if (self::getSportCategoriesByResultCount(1134, 4, 8, 'Ж') !== '2ю') {
            throw new Exception('Непраивльно считаются разряды');
        }

        if (self::getSportCategoriesByResultCount(1040, 4, 8, 'Ж') !== '3ю') {
            throw new Exception('Непраивльно считаются разряды');
        }

        if (self::getSportCategoriesByResultCount(1106.12, 8, 8, 'Ж') !== '2ю') {
            throw new Exception('Непраивльно считаются разряды');
        }

        if (self::getSportCategoriesByResultCount(1140, 4, 8, 'Ж') !== '2ю') {
            throw new Exception('Непраивльно считаются разряды');
        }

        if (self::getSportCategoriesByResultCount(1039.86, 4, 8, 'Ж') !== '3ю') {
            throw new Exception('Непраивльно считаются разряды');
        }

        if (self::getSportCategoriesByResultCount(1736.78, 7, 10, 'М') !== '1') {
            throw new Exception('Непраивльно считаются разряды');
        }

        if (self::getSportCategoriesByResultCount(1942.89, 5.5, 9, 'М') !== '1') {
            throw new Exception('Непраивльно считаются разряды');
        }

        echo '<br> testSportCategoriesByResultCount end2';

    }
}

//SportsCategories::testSportCategoriesByResultCount();