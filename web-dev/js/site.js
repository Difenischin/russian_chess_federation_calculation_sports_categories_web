$(function () {
    $('.last-tournaments ').on('click', 'a', function () {
        if (this.name !== "") {
            $('#statistic-tournament_url').val(this.name);
            $('button[name="contact-button"]').click();
        }
    });
    $('button[name="contact-button"]').on('click', 'a', function () {

    });
    if (isElementInDom('#categories')) {
        addDatatables();
    }
    function isElementInDom(selector) {
        return $(selector).length > 0;
    }

    function addDatatables() {
        var language = {
            "processing": "Подождите...",
            "search": "Поиск:",
            "lengthMenu": "Показать _MENU_ записей",
            "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
            "infoEmpty": "Записи с 0 до 0 из 0 записей",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "infoPostFix": "",
            "loadingRecords": "Загрузка записей...",
            "zeroRecords": "Записи отсутствуют.",
            "emptyTable": "В таблице отсутствуют данные",
            "paginate": {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Следующая",
                "last": "Последняя"
            },
            "aria": {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
            }
        };
        $('#categories').DataTable({
            language : language,
            paging:   false,
            order: [[ 3, "desc" ]]
        });
    }
});